import api from './api';

export default {
  namespaced: true,
  state: {
    auth: null,
  },
  getters: {
    auth: (state) => {
      return state.auth || JSON.parse(localStorage.getItem('auth'));
    }
  },
  mutations: {
    ACCESS_GRANTED: (state, payload) => {
      localStorage.setItem('auth', JSON.stringify(state.auth = payload));
    }
  },
  actions: {
    authenticate: async ({ commit }, credentials) => {
      const response = await api.login(credentials);
      if (response.error) {

      }
      commit('ACCESS_GRANTED', response);
    }
  }
}
