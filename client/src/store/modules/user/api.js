import config from '@/config/app';
import axios from 'axios';

export default {
  login({ username, password }) {
    const payload = { ...config.auth, username, password, };

    return axios.post(`${config.server}/oauth/token`, payload)
      .then(response => {
        // instance.defaults.timeout = 2500;
        return response.data;
      });
  }
}
