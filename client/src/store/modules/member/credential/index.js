import { credential as api } from '../api';
import { credential as schema } from '../schema';
import resource from '@/store/_resource';

export default resource({ module: 'member/credential', api, schema }, {
  getters: {
    params: (state, getters, { member }) => {
      return { member: member.current }
    },
  },
  actions: {
    update: async ({ dispatch }, payload) => {
      return dispatch('store', payload);
    },
  },
});
