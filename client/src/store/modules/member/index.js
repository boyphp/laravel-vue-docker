import { filter } from 'lodash';
import resource from '@/store/_resource';
import api from './api';
import schema from './schema';

import credential from './credential';
import education from './education';
import hospital from './hospital';
import internship from './internship';
import position from './position';
import induction from './induction';
import society from './society';
// import exam from './exam';

export default resource({ module: 'member', api, schema }, {
  modules: {
    credential,
    hospital,
    education,
    internship,
    position,
    induction,
    society,
    // exam,
  },
  getters: {
    mutate: (state, { root }) => member => {
      return {...member,
        name: filter([member.first_name, member.last_name]).join(' '),
        address: root['address/find'](member.address),
        contact: root['contact/find'](member.contact),
      }
    },
  },
});
