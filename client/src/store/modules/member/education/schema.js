import { schema } from 'normalizr';
import institution from '../../institution/schema';
import degree from '../../catalog/education-degree/schema';

export default new schema.Entity('member/education', {
  institution: institution,
  degree: degree,
});
