import { schema } from 'normalizr';
import address from '../address/schema';
import contact from '../contact/schema';
import nationality from '../nationality/schema';
import maritalStatus from '../catalog/marital-status/schema';
import membershipStatus from '../catalog/membership-status/schema';
import chapter from '../catalog/member-chapter/schema';
import category from '../catalog/member-category/schema';
import subspecialty from '../catalog/subspecialty/schema';

import hospital from './hospital/schema';
import education from './education/schema';
import internship from './internship/schema';
import position from './position/schema';
import induction from './induction/schema';
import society from './society/schema';


export const credential = new schema.Entity('member/credential');

export default new schema.Entity('member', {
  credential: credential,
  address: address,
  contact: contact,
  nationality: nationality,
  marital_status: maritalStatus,
  membership_status: membershipStatus,
  chapter: chapter,
  category: category,
  subspecialty: subspecialty,

  hospitals: [ hospital ],
  education: [ education ],
  internships: [ internship ],
  positions: [ position ],
  inductions: [ induction ],
  societies: [ society ],
  // exams: [ exam ],
});
