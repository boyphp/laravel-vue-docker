import resource from '@/api/_resource';

export default resource('members', {
  fetch(query) {
    return axios.get('members', { params: { query } })
      .then(response => response.data);
  },
});

export const credential = resource('members/{member}/credential');

export const education = resource('members/{member}/education');
export const internship = resource('members/{member}/internships');
export const hospital = resource('members/{member}/hospitals');
export const position = resource('members/{member}/positions');
export const induction = resource('members/{member}/inductions');
export const society = resource('members/{member}/societies');
export const exam = resource('members/{member}/exams');
