import { schema } from 'normalizr';
import hospital from '../../hospital/schema';

export default new schema.Entity('member/internship', {
  hospital: hospital,
  residency: hospital
});
