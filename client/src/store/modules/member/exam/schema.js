import { schema } from 'normalizr';
import exam from '../../exam/schema';
import status from '../../catalog/member-exam-status/schema';

export default new schema.Entity('member/exam', {
  exam: exam,
  status: status,
});
