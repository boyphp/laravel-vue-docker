import { exam as api } from '../../member/api';
import schema from './schema';
import resource from '@/store/_resource';

export default resource({ module: 'member/exam', api, schema }, {
  getters: {
    params: (state, getters, { member }) => {
      return { member: member.current }
    },
  },
});
