import { fromPairs, map } from 'lodash';
import Vue from 'vue';
import Vuex from 'vuex';
import normalizr from './plugins/normalizr';

const request = require.context('./modules', true, /^.\/[^/]+\/index\.js$/);
const modules = fromPairs(request.keys().map(file => {
  return [file.split`/`[1], request(file).default];
}));

Vue.use(Vuex);
export default new Vuex.Store({
  modules: modules,
  plugins: [normalizr],
  actions: {
    preload: ({ dispatch }, modules) => {
      return Promise.all(map(modules, item => dispatch(`${item}/preload`)));
    },
  },
  strict: process.env.NODE_ENV !== 'production'
})
