import { merge, get, without, toString } from 'lodash';
import { normalize } from 'normalizr';

export default function({ module, api, schema }, store) {
  // set primary key, possibly change to uuid
  const KEY = 'id';

  const getters = {
    params: () => {
      return {}
    },
    find: ({ entities }, { mutate }) => id => {
      return mutate(get(entities, id, {}));
    },
    result: ({ result }, getters) => {
      return result.map(id => getters.find(id));
    },
    current: ({ current }, getters) => {
      return getters.find(current);
    },
    mutate: () => data => {
      return data;
    },
    root: (state, getters, rootState, rootGetters) => {
      return rootGetters;
    }
  }

  const mutations = {
    SET_RESULT: (state, payload) => {
      state.result = payload.result;
    },
    SET_RESULT_ENTITIES: (state, entities) => {
      state.entities = entities;
      state.result = Object.keys(state.entities);
      // console.log('SET', module, state.entities);
    },
    MERGE_RESULT: (state, payload) => {
      state.result = [...state.result, ...payload.result];
    },
    MERGE_RESULT_ENTITIES: (state, entities) => {
      state.entities = {...state.entities, ...entities};
      state.result = Object.keys(state.entities);
      // console.log('MERGE', module, state.entities);
    },
    SET_CURRENT: (state, current) => {
      state.current = current;
    },
    UNSET_RESULT: (state, payload) => {
      state.result = without(state.result, toString(payload));
    },
  }

  const actions = {
    preload: async ({ commit, dispatch, getters }, query) => {
      return new Promise((resolve, reject) => {
        /*if (getters.result.length) {
          return resolve(getters.result);
        }*/
        dispatch('query', query).then(response => {
          // commit('PRELOADED', true);
          return resolve(response);
        });
      });
    },
    query: async ({ commit, getters }, query) => {
      return new Promise((resolve, reject) => {
        api.query(query, getters.params).then(response => {
          commit('MERGE_RESULT', normalize(response, [schema]));
          return resolve(getters.result);
        });
      });
    },
    show: async ({ commit, getters }, id) => {
      return new Promise((resolve, reject) => {
        commit('SET_CURRENT', id);
        api.show(id).then(response => {
          commit('MERGE_RESULT', { schema, ...normalize(response, schema) });
          return resolve(getters.current);
        })
      });
    },
    save: async ({ commit, getters, dispatch }, payload) => {
      return new Promise((resolve, reject) => {
        // handle store or update by checking primary key
        const action = !payload[KEY] ? 'store' : 'update';
        dispatch(action, payload).then(response => {
          // reject if validation / authorization failed
          if (response.error) {
            return reject(response.error);
          }

          commit('MERGE_RESULT', { schema, ...response = normalize(response, schema) });
          return resolve(getters.find(response.result)); // resolve(true);
        });
      });
    },
    store: async ({ commit, getters }, payload) => {
      return api.store(payload, getters.params);
    },
    update: async ({ commit, getters }, payload) => {
      return api.update(payload, getters.params);
    },
    /*
    delete: async ({ commit, getters }, payload) => {
      const response = await api.destroy(payload, getters.params);
      if (response.deleted) {
        commit('DATA_DELETE_SUCCESS', payload.id);
        // commit('ENTITY_DELETED', { [module]: payload.id }, { root: true });
      }
    }
    */
  }

  return merge({
    namespaced: true,
    state: {
      entities: {},
      result: [],
      current: null,
    },
    getters,
    mutations,
    actions,
  }, store);
}
