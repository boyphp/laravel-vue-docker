import Layout from './Index.vue';
import Breadcrumbs from './Breadcrumbs';

export {
  Breadcrumbs,
}

export default Layout;
