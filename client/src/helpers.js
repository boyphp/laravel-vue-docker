window._ = require('lodash');

window.moment = require('moment');
require('twix');

window.swal = require('sweetalert');
window.sweetalert = {
  confirm(callback, options = {}) {
    return swal({
      title: "Are you sure?",
      text: "You will not be able to recover this data!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it!",
      ...options,
    }, () => callback());
  }
}

window.$ = window.jQuery = require('jquery');
window.Bloodhound = require('corejs-typeahead');
require('bootstrap');
require('selectize');
require('fullcalendar');
