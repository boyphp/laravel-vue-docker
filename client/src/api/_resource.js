import rest from './auth';

export default function(prefix, extra) {

  return {
    http: rest,
    replace(params) {
      return prefix.replace(/\{(.+)\}/g, (match, key) => {
        return params[key];
      });
    },
    query(query, params) {
      return this.http.get(this.replace(params), { params: { query } })
        .then(response => response.data);
    },
    show(payload, params = {}) {
      return this.http.get(`${this.replace(params)}/${payload}`)
        .then(response => response.data);
    },
    store(payload, params) {
      return this.http.post(this.replace(params), payload)
        .then(response => response.data)
        .catch(({ response }) => {
          if (response.status == 422) { // Unprocessable Entity
            return { error: response.data };
          }
        });
    },
    update(payload, params) {
      return this.http.put(`${this.replace(params)}/${payload.id}`, payload)
        .then(response => response.data)
        .catch(({ response }) => {
          if (response.status == 422) { // Unprocessable Entity
            return { error: response.data };
          }
        });
    },
    destroy(payload, params) {
      return this.http.delete(`${this.replace(params)}/${payload.id}`, payload)
        .then(response => response.data);
    },
    ...extra
  }
}
