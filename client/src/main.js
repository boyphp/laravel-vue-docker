require('./sass/app.scss');
require('./helpers');

import Vue from 'vue';
import App from './App';
import router from './routes';
import store from './store';

Vue.config.productionTip = false;
new Vue({
  el: '#root',
  router,
  store,
  template: `<app />`,
  components: { App }
});
