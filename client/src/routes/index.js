import Vue from 'vue';
import VueRouter from 'vue-router';
import maintenance from './maintenance';

Vue.use(VueRouter);
export default new VueRouter({
  mode: 'history',
  routes: [
    { path: '', component: require('@/components/Auth') },
    { path: '/home', component: require('@/components/Home') },
    { path: '/members', component: require('@/components/Members') },
    {
      path: '/members/:id',
      component: require('@/components/Member'),
      props: true,
      children: [
        { path: '', name: 'member-overview', component: require('@/components/Member/Overview') },
        { path: 'information', name: 'member-info', component: require('@/components/Member/Information') },
        { path: 'dues', name: 'member-due', component: require('@/components/Member/Due') },
        { path: 'payments', name: 'member-payment', component: require('@/components/Member/Payment') },
        // { path: 'certification', component: require('@/components/Member/Certification') },
      ]
    },
    { path: '/activities', component: require('@/components/Activities') },
    { path: '/activities/:id', component: require('@/components/Activity/Show'), props: true },
    { path: '/payments', component: require('@/components/Payments') },
    { path: '/maintenance', name: 'maintenance', component: require('@/components/Maintenance'), children: maintenance },
    /*
    {
      path: '/reports',
      component: require('@/components/Reports'),
      children: [
        { path: 'summary-of-membership', component: require('@/components/Reports/summary-of-membership') },
        { path: 'non-member-list', component: require('@/components/Reports/non-member-list') },
        { path: 'affiliates-member-list', component: require('@/components/Reports/affiliates-member-list') },
        { path: 'regular-fellows', component: require('@/components/Reports/regular-fellows') },
        { path: 'list-of-attendees', component: require('@/components/Reports/list-of-attendees') },
      ]
    },
    */
    // { path: '*', component: NotFoundComponent }
  ]
});
