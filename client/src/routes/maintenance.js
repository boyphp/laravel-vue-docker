export const modules = [
  { path: 'countries', title: 'Countries', store: 'country', },
  { path: 'regions', title: 'Regions', store: 'region', },
  { path: 'provinces', title: 'Provinces', store: 'province', },
  { path: 'cities', title: 'Cities', store: 'city', },
  { path: 'locations', title: 'Locations', store: 'location', },
  { path: 'nationalities', title: 'Nationalities', store: 'nationality', },
  { path: 'marital-status', title: 'Marital Status', store: 'catalog/marital-status', },
  { path: 'membership-status', title: 'Membership Status', store: 'catalog/membership-status', },
  { path: 'chapters', title: 'Member Chapters', store: 'catalog/member-chapter', },
  { path: 'categories', title: 'Member Categories', store: 'catalog/member-category', },
  // { path: 'specialties', title: 'Member Specialties', store: 'catalog/specialty', },
  { path: 'subspecialties', title: 'Member Subspecialties', store: 'catalog/subspecialty', },
  { path: 'position-categories', title: 'Position Categories', store: 'position-category', },
  { path: 'societies', title: 'Societies', store: 'society', },
  { path: 'member-society-status', title: 'Society Affiliation Status', store: 'catalog/member-society-status', },
  { path: 'institutions', title: 'Institutions', store: 'institution', },
  { path: 'education-degrees', title: 'Education Degrees', store: 'catalog/education-degree', },
  { path: 'exams', title: 'Exams', store: 'exam', },
  { path: 'exam-types', title: 'Exam Types', store: 'catalog/exam-type',  },
  { path: 'exam-categories', title: 'Exam Categories', store: 'catalog/exam-category',  },
  { path: 'member-exam-status', title: 'Member Exam Status', store: 'catalog/member-exam-status',  },
  { path: 'activity-types', title: 'Activity Types', store: 'catalog/activity-type', },
  { path: 'hospitals', title: 'Hospitals', store: 'hospital', },

  // FINANCE
  { path: 'fiscal-periods', title: 'Fiscal Periods', store: 'finance/fiscal-period', },
  { path: 'payer-types', title: 'Payer Types', store: 'finance/catalog/payer-type', },
  { path: 'payment-mode', title: 'Payment Mode', store: 'finance/catalog/payment-mode', },
  { path: 'sponsors', title: 'Sponsors', store: 'finance/sponsor', },
  { path: 'fees', title: 'Fees', store: 'finance/fee', },
]

export default modules.map(module => {
  return {
    name: module.name || module.path,
    path: module.path,
    component: require('@/components/Maintenance/Module'),
    props: { default: true, module: module.path },
    meta: module,
  }
});
