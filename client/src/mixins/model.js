import { transform, set } from 'lodash';

export default {
  data() {
    return {
      model: null,
      errors: {},
      notification: null,
      messages: {
        success: 'Record has been saved.',
        deleted: 'Record has been deleted.',
      },
    }
  },
  computed: {
    form() {
      return {}
    }
  },
  methods: {
    submit(model) {
      this.save(model || {}).then(response => {
        this.notify(this.messages['success']);
        this.edit(response);
        // this.clear();
      }).catch(errors => {
        this.errors = transform(errors, (result, value, key) => set(result, key, value));
      });
    },
    remove(model) {
      sweetalert.confirm(() => {
        /*
        this.delete(model).then(response => {
          this.notify(this.messages['deleted'])
          this.clear();
        });
        */
      });
    },
    create() {
      this.model = { ...this.form };
      this.errors = {};
    },
    edit(model) {
      this.model = model;
      this.errors = {};
    },
    clear() {
      this.model = null;
      this.errors = {};
    },
    notify(message) {
      this.notification = message;
    },
  }
}
