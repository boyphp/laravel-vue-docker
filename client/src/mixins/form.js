import { isEqual, clone } from 'lodash';

export default {
  props: {
    value: {
      type: Object,
    },
    errors: {
      type: Object,
      default() {
        return {}
      }
    }
  },
  data() {
    return {
      model: this.create(),
    }
  },
  methods: {
    create() {
      return {}
    },
  },
  watch: {
    value(model) {
      if (!isEqual(this.model, model)) {
        this.model = Object.assign({}, this.create(), model);
        // this.model = clone(model);
      }
    },
    model: {
      deep: true,
      handler(model) {
        this.$emit('input', model);
      }
    }
  }
}
