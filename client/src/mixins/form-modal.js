export default {
  watch: {
    value(value) {
      this[value ? 'open' : 'close']();
      // this[!_.isEmpty(value) ? 'open' : 'close']();
    }
  },
  methods: {
    open() {
      this.$refs['modal'].show();
    },
    close() {
      // this.$emit('cancel');
      this.$refs['modal'].hide();
    }
  }
}
