cd ~
sudo yum update
sudo yum install -y yum-utils git
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum makecache fast

sudo yum -y install docker-ce
sudo systemctl start docker

curl -L "https://github.com/docker/compose/releases/download/1.11.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# git source code
git clone https://ddudz@bitbucket.org/ddudz/pcp-drm.git
cd pcp-drm

# run docker
docker-compose up -d --build

# initial setup
docker-compose exec php bash -c "composer install \
 && chmod -R o+w bootstrap/cache/ storage/ \
 && cp .env.example .env \
 && php artisan key:generate"

# setup database
docker-compose exec php bash -c "composer update && php artisan migrate --seed"





# PCPDRM SETUP by: Ronald
#-----------------------------------------------------------------------------------
#
# create directory for e.g at drive d:\
> cd d:
> mkdir pcp-drm

# go to directory then clone repository
> cd pcp-drm
> git clone https://ddudz@bitbucket.org/ddudz/pcp-drm.git .

# however, with slow connection run this, otherwise skip
> git clone https://ddudz@bitbucket.org/ddudz/pcp-drm.git . --depth 1
> git fetch --unshallow

# install nodejs at https://nodejs.org/en/ then restart
# install npm packages and build client application
> cd client
> npm install -g npm
> npm install
> npm run build

# run docker in root directory
> cd ..
> docker-compose up -d --build

# setup/install laravel packages in php container
> winpty docker-compose exec php bash
> composer install

# set write permissions to some folders, 
> chmod -R o+w bootstrap/cache/ storage/

# generate application key and setup database
> cp .env.example .env
> php artisan key:generate
> php artisan migrate --seed
> php artisan passport:install

# visit http://localhost in your browser
#
#-----------------------------------------------------------------------------------
#
# running docker after restart or turning on pc
> cd pcp-drm
> docker-compose up -d build

# updating the repository from git commits/updates
> cd pcp-drm
> git pull

# rebuid client application
> npm install
> npm run build

# install packages, rerun mysql database migrations 
# (migration here does not include migration from old database to new database,
# only changes made to database such as adding new tables or altering tables)
> winpty docker-compose exec php bash
> composer install
> php artisan migrate:refresh --seed
# or
> php artisan migrate

#
#-----------------------------------------------------------------------------------
#
# mysql connection details
# hostname: localhost
# port: 3306
# username: root
# password: password
# database: pcpdrm






