@extends('layouts.metronic')

@section('content')
  {{ session('message') }}
	<h3>ROLE</h3>
	<a href="{{ route('role.create') }}" class="btn btn-success" >Create Role</a>
	<br><br>
	<table width="100%" class="table">
		<thead>
		<tr>
			<th>Name</th>
			<th>Display Name</th>
			<th>Description</th>
			<th>Action</th>
		 </tr>
		</thead>
		<tbody>
		@forelse($roles as $role)
		<tr>
			<td>{{ $role->name }}</td>
			<td>{{ $role->display_name }}</td>
			<td>{{ $role->description }}</td>
			<td>
				<a href="{{ route('role.edit',$role->id) }}" class="btn btn-info btn-sm" >Edit</a>
				<form action="{{ route('role.destroy',$role->id) }}" method="post">
				{{ csrf_field() }}
				{{ method_field('DELETE') }}
				<input class="btn btn-sm btn-danger" type="Submit" value="DELETE">
				</form>
			</td>
		</tr>
		@empty
			<td colspan="3">No Roles</td>
		@endforelse
		</tbody>
	</table>
@endsection