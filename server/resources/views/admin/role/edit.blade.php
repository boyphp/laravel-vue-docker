@extends('layouts.metronic')

@section('content')
	<form action="{{ route('role.update',$role->id) }}" method="POST">
	{{ csrf_field ()}}
	{{ method_field('PATCH')}}
	<legend>Edit Role</legend>
		<div class="form-group"> 
			<input class="form-control" type="text" name="name" placeholder="Role Name" value="{{ $role->name }}">
		</div>
		<div class="form-group"> 
			<input class="form-control" type="text" name="display_name" placeholder="Display Name" value="{{ $role->display_name }}"  >
		</div>
		<div class="form-group"> 
			<input class="form-control" type="text" name="description" placeholder="Description" value="{{ $role->description }}" >
		</div>
				<div class="form-group"> 
			<h3>Module Permission</h3>
			<table class="table">
				@forelse($modules as $module)
					<tr>
						<td colspan="2"><h4>{{ $module->name }}</h4></td>	
					</tr>
					@foreach($module->permissions as $permission)
					 <tr>
						<td class="checkbox">
							<label><input  {{in_array($permission->id,$role_permissions)?"checked":"" }} type="checkbox" name="permission[]" value={{ $permission->id }}>{{ $permission->name }} </label>	
						</td>
					 </tr>
					 @endforeach	
		         @empty
		         <tr>
					<td colspan="3">No Module</td>
				</tr>
				@endforelse		
			</table>
		</div>
		<div>
			<input type="submit" value="Submit" class="btn btn-success">
		</div>
	</form>
@endsection