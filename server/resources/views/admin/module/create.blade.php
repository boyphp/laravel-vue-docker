@extends('layouts.metronic')

@section('content')
	<form action="{{ route('module.store') }}" method="POST">
	{{ csrf_field ()}}
	<legend>Create Module</legend>
		<div class="form-group"> 
			<input class="form-control" type="text" name="name" placeholder="Module Name">
		</div>
		<div class="form-group"> 
			<input class="form-control" type="text" name="description" placeholder="Description" >
		</div>
		<div>
			<input type="submit" value="Submit" class="btn btn-success">
		</div>
	</form>
@endsection