@extends('layouts.metronic')

@section('content')
	<form action="{{ route('module.update',$module->id) }}" method="POST">
	{{ csrf_field ()}}
	{{ method_field('PATCH')}}
	<legend>Update Module</legend>
		<div class="form-group"> 
			<input class="form-control" type="text" name="name" placeholder="Module Name" value="{{ $module->name }}">
		</div>
		<div class="form-group"> 
			<input class="form-control" type="text" name="description" placeholder="Description" value="{{ $module->description }}" >
		</div>
		<div>
			<input type="submit" value="Submit" class="btn btn-success">
		</div>
	</form>
@endsection