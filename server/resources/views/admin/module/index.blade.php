@extends('layouts.metronic')

@section('content')
  {{ session('message') }}
	<h3>Modules</h3>
	<a href="{{ route('module.create') }}" class="btn btn-success" >Create Module</a>
	<br><br>
	<table width="100%" class="table">
		<thead>
		<tr>
			<th>Name</th>
			<th>Action</th>
		 </tr>
		</thead>
		<tbody>
		@forelse($modules as $module)
		<tr>
			<td>{{ $module->name }}</td>
			<td>{{ $module->description }}</td>
			<td>
				<a href="{{ route('module.edit',$module->id) }}" class="btn btn-info btn-sm" >Edit</a>
				<form action="{{ route('module.destroy',$module->id) }}" method="post">
				{{ csrf_field() }}
				{{ method_field('DELETE') }}
				<input class="btn btn-sm btn-danger" type="Submit" value="DELETE">
				</form>
			</td>
		</tr>
		@empty
			<td colspan="3">No Modules</td>
		@endforelse
		</tbody>
	</table>
@endsection