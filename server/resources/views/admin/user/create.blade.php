@extends('layouts.metronic')

@section('content')
	<form action="{{ route('user.store') }}" method="POST">
	{{ csrf_field ()}}
	<legend>Create User</legend>
		<div class="form-group"> 
			<input class="form-control" type="text" name="name" placeholder="User Name">
		</div>
		<div class="form-group"> 
			<input class="form-control" type="text" name="email" placeholder="Email Address" >
		</div>
		<div class="form-group"> 
			<input class="form-control" type="password" name="password" placeholder="Password" >
		</div>
		<div class="form-group"> 
			<h3>Roles</h3>
			@foreach($roles as $role)
				<div class="checkbox">
					<label><input type="checkbox" name="roles[]" value={{ $role->id }}>{{ $role->name }} </label>
			    </div>
			@endforeach	
		</div>
		<div>
			<input type="submit" value="Submit" class="btn btn-success">
		</div>
	</form>
@endsection