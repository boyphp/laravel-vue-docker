@extends('layouts.metronic')

@section('content')
	<form action="{{ route('user.update',$user->id) }}" method="POST">
	{{ csrf_field ()}}
	{{ method_field('PATCH')}}	
	<legend>Update User</legend>
		<div class="form-group"> 
			<input class="form-control" type="text" name="name" placeholder="User Name" value="{{ $user->name }}">
		</div>
		<div class="form-group"> 
			<input class="form-control" type="text" name="email" placeholder="Email Address" value="{{ $user->email }}" >
		</div>
		<div class="form-group"> 
			<input class="form-control" type="password" name="password" placeholder="Password" >
		</div>
		<div class="form-group"> 
			<h3>Roles</h3>
			@foreach($roles as $role)
				<div class="checkbox">
					<label><input type="checkbox" name="roles[]" {{in_array($role->id,$role_user)?"checked":"" }} value={{ $role->id }}>{{ $role->name }} </label>
			    </div>
			@endforeach	
		</div>
		<div>
			<input type="submit" value="Submit" class="btn btn-success">
		</div>
	</form>
@endsection