@extends('layouts.metronic')

@section('content')
  {{ session('message') }}
	<h3>Users</h3>
	<a href="{{ route('user.create') }}" class="btn btn-success" >Create User</a>
	<br><br>
	<table width="100%" class="table">
		<thead>
		<tr>
			<th>Name</th>
			<th>Email</th>
			<th>Roles</th>
			<th>Action</th>
		 </tr>
		</thead>
		<tbody>
		@forelse($users as $user)
		<tr>
			<td>{{ $user->name }}</td>
			<td>{{ $user->email }}</td>
			<td>
			  @foreach($user->roles as $role)
                        {{$role->name}}
              @endforeach
			</td>
			<td>
				<a href="{{ route('user.edit',$user->id) }}" class="btn btn-info btn-sm" >Edit</a>
				<form action="{{ route('user.destroy',$user->id) }}" method="post">
				{{ csrf_field() }}
				{{ method_field('DELETE') }}
				<input class="btn btn-sm btn-danger" type="Submit" value="DELETE">
				</form>
			</td>
		</tr>
		@empty
			<td colspan="3">No Roles</td>
		@endforelse
		</tbody>
	</table>
@endsection