@extends('layouts.metronic')

@section('content')
  {{ session('message') }}
	<h3>Permisssion</h3>
	<a href="{{ route('permission.create') }}" class="btn btn-success" >Create Permission</a>
	<br><br>
	<table width="100%" class="table">
		<thead>
		<tr>
			<th>Name</th>
			<th>Display Name</th>
			<th>Description</th>
			<th>Action</th>
		 </tr>
		</thead>
		<tbody>
		@forelse($permissions as $permission)
		<tr>
			<td>{{ $permission->name }}</td>
			<td>{{ $permission->display_name }}</td>
			<td>{{ $permission->description }}</td>
			<td>
				<a href="{{ route('permission.edit',$permission->id) }}" class="btn btn-info btn-sm" >Edit</a>
				<form action="{{ route('permission.destroy',$permission->id) }}" method="post">
				{{ csrf_field() }}
				{{ method_field('DELETE') }}
				<input class="btn btn-sm btn-danger" type="Submit" value="DELETE">
				</form>
			</td>
		</tr>
		@empty
			<td colspan="3">No Roles</td>
		@endforelse
		</tbody>
	</table>
@endsection