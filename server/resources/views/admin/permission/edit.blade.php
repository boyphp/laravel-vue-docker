@extends('layouts.metronic')

@section('content')
	<form action="{{ route('permission.update',$permission->id) }}" method="POST">
	{{ csrf_field ()}}
	{{ method_field('PATCH')}}
	<legend>Edit Permission</legend>
		<div class="form-group"> 
			<input class="form-control" type="text" name="name" placeholder="Name" value="{{ $permission->name }}">
		</div>
		<div class="form-group"> 
			<input class="form-control" type="text" name="display_name" placeholder="Display Name" value="{{ $permission->display_name }}" >
		</div>
		<div class="form-group"> 
			<input class="form-control" type="text" name="description" placeholder="Descritpion" value="{{ $permission->description }}" >
		</div>
		
	   <div class="form-group"> 
			<h3>Module</h3>
			<select name="module_id" class="form-control">
			@foreach($modules as $module)
				<option  {{in_array($module->id,$module_permission)?"selected":"" }}  value="{{ $module->id }}">{{ $module->name }}</option>
			@endforeach	
		    </select>	
		</div>
		<div>
			<input type="submit" value="Submit" class="btn btn-success">
		</div>
	</form>
@endsection