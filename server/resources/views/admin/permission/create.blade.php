@extends('layouts.metronic')

@section('content')
	<form action="{{ route('permission.store') }}" method="POST">
	{{ csrf_field ()}}
	<legend>Create Permission</legend>
		<div class="form-group"> 
			<input class="form-control" type="text" name="name" placeholder="Name">
		</div>
		<div class="form-group"> 
			<input class="form-control" type="text" name="display_name" placeholder="Display Name" >
		</div>
		<div class="form-group"> 
			<input class="form-control" type="text" name="description" placeholder="Descritpion" >
		</div>
	   <div class="form-group"> 
			<h3>Module</h3>
			<select name="module_id" class="form-control">
			@foreach($modules as $module)
				<option value="{{ $module->id }}">{{ $module->name }}</option>
			@endforeach	
		    </select>	
		</div>
		<div>
			<input type="submit" value="Submit" class="btn btn-success">
		</div>
	</form>
@endsection