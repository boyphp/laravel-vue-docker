<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Member::class, function (Faker\Generator $faker) {
    static $chapters, $categories, $status;

    $chapters = $chapters ?: App\Models\MemberChapter::pluck('id');
    $categories = $categories ?: App\Models\MemberCategory::pluck('id');
    $status = $status ?: App\Models\MembershipStatus::pluck('id');

    return [
        'title' => $faker->optional()->title(['male', 'female'][$gender = rand(0, 1)]),
        'first_name' => $faker->firstName(['male', 'female'][$gender]),
        'last_name' => $faker->lastName,
        'middle_name' => $faker->optional()->lastName,
        'extension_name' => $faker->optional()->suffix,
        'gender' => 'MF'[$gender],
        'birthday' => $faker->dateTimeInInterval('-80 years', '-20 years'),
        'spouse_name' => $faker->optional()->name(['female', 'male'][$gender]),
        'membership_status_id' => $status->random(),
        'chapter_id' => $chapters->random(),
        'category_id' => $categories->random(),
    ];
});

$factory->define(App\Models\MemberCredential::class, function (Faker\Generator $faker) {
    return [
        'prc_no' => $faker->unique()->randomNumber,
        'prc_date_issued' => $faker->date,
    ];
});

$factory->define(App\Models\Contact::class, function (Faker\Generator $faker) {
    return [
        'email' => $faker->email,
        'office_no' => $faker->tollFreePhoneNumber,
        'mobile_no' => $faker->e164PhoneNumber,
        'secondary_email' => $faker->optional()->email,
    ];
});

$factory->define(App\Models\Address::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->streetAddress,
        'location' => $faker->secondaryAddress,
        'city' => $faker->city,
        'province' => $faker->state,
        'country' => $faker->country,
        'zip_code' => $faker->postcode,
    ];
});

$factory->define(App\Models\Finance\Payment::class, function (Faker\Generator $faker) {
    static $members, $chapters, $type, $mode;

    $members = $members ?: App\Models\Member::onlyWith()->get();
    $chapters = $chapters ?: App\Models\MemberChapter::pluck('id');
    $type = $type ?: App\Models\Finance\PayerType::first();
    $mode = $mode ?: App\Models\Finance\PaymentMode::first();

    return [
        'or_no' => $faker->unique()->randomNumber(6),
        'payment_date' => $faker->dateTimeThisYear,
        'chapter_id' => $chapters->random(),
        'member_id' => ($member = $members->random())->id,
        'payer_type_id' => $type->id,
        'payer_name' => "{$member->first_name} {$member->last_name}",
        'amount' => ($amount = $faker->numberBetween(0, 100)) * 100,
        'total_amount' => $amount * 100,
        'mode_id' => $mode->id,
        'bank_branch' => $faker->optional()->company,
        'check_no' => $check = $faker->optional()->randomNumber(6),
        'check_date' => $check ? $faker->dateTimeThisMonth->format('Y-m-d') : null,
        'check_amount' => $check ? $faker->numberBetween(0, $amount) * 100 : null,
    ];
});

$factory->define(App\Models\Activity::class, function (Faker\Generator $faker) {
    static $types;

    $types = $types ?: App\Models\ActivityType::pluck('id');

    return [
        'name' => $faker->catchPhrase,
        'date_from' => $from = $faker->dateTimeBetween('-2 months', '+1 months'),
        'date_to' => $faker->dateTimeInInterval($from, '+ 5 days'),
        'venue' => $faker->address,
        'units' => $faker->numberBetween(0, 20) * 10,
        'type_id' => $types->random(),
        'organizer' => $faker->company . ' ' . $faker->companySuffix,
        'remarks' => $faker->sentence,
    ];
});
