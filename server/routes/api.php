<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function() {
    // MEMBER
    Route::get('/members/summary/category', 'MemberController@summaryCategory');
    Route::get('/members/recent', 'MemberController@recent');
    Route::resource('/members', 'MemberController', ['except' => ['create', 'edit']]);

    Route::group(['prefix' => '/members/{member}'], function() {
        Route::resource('/credential', 'MemberCredentialController', ['only' => ['index', 'store']]);
        Route::resource('/hospitals', 'MemberHospitalController', ['except' => ['create', 'edit']]);
        Route::resource('/education', 'MemberEducationController', ['except' => ['create', 'edit']]);
        Route::resource('/internships', 'MemberInternshipController', ['except' => ['create', 'edit']]);
        Route::resource('/positions', 'MemberPositionController', ['except' => ['create', 'edit']]);
        Route::resource('/inductions', 'MemberInductionController', ['except' => ['create', 'edit']]);
        Route::resource('/societies', 'MemberSocietyController', ['except' => ['create', 'edit']]);

        // Route::resource('/exams', 'MemberExamController', ['except' => ['create', 'edit']]);
        // Route::resource('/activities', 'MemberActivityController');
    });

    // ACTIVITY
    Route::resource('/activities', 'ActivityController');
    Route::group(['prefix' => '/activities/{activity}'], function() {
        Route::resource('/members', 'ActivityMemberController', ['only' => ['update', 'destroy']]);
    });

    // ACTIVITY TYPE
    Route::resource('/activity-types', 'ActivityTypeController', ['parameters' => ['activity-types' => 'type']]);

    // PLACE
    Route::resource('/countries', 'CountryController', ['except' => ['create', 'edit']]);
    Route::resource('/provinces', 'ProvinceController', ['except' => ['create', 'edit']]);
    Route::resource('/cities', 'CityController', ['except' => ['create', 'edit']]);
    Route::resource('/regions', 'RegionController');
    Route::resource('/locations', 'LocationController');
    Route::resource('/nationalities', 'NationalityController');

    // CATALOG
    Route::resource('/chapters', 'ChapterController');
    Route::resource('/categories', 'CategoryController');
    Route::resource('/marital-status', 'MaritalStatusController', ['parameters' => ['marital-status' => 'status']]);
    Route::resource('/membership-status', 'MembershipStatusController', ['parameters' => ['membership-status' => 'status']]);
    Route::resource('/subspecialties', 'SubspecialtyController');

    // HOSPITAL
    Route::resource('/hospitals', 'HospitalController');

    // EDUCATION
    Route::resource('/education-degrees', 'EducationDegreeController', ['parameters' => ['education-degrees' => 'degree']]);
    Route::resource('/institutions', 'InstitutionController');

    // POSITION
    Route::resource('/position-categories', 'PositionCategoryController', ['parameters' => ['position-categories' => 'category']]);

    // SOCIETY
    Route::resource('/societies', 'SocietyController', ['except' => ['create', 'edit']]);
    Route::resource('/member-society-status', 'MemberSocietyStatusController', ['parameters' => ['member-society-status' => 'status']]);

    // EXAM
    // Route::resource('/exams', 'ExamController');
    // Route::resource('/exam-types', 'ExamTypeController');
    // Route::resource('/exam-categories', 'ExamCategoryController');
    // Route::resource('/exam-status', 'ExamStatusController');

    // FINANCE
    Route::group(['prefix' => '/finance'], function() {
        Route::resource('/fiscal-periods', 'Finance\FiscalPeriodController', ['parameters' => ['fiscal-periods' => 'period']]);
        Route::resource('/payer-types', 'Finance\PayerTypeController', ['parameters' => ['payer-types' => 'type']]);
        Route::resource('/payment-modes', 'Finance\PaymentModeController', ['parameters' => ['payment-modes' => 'mode']]);
        Route::resource('/sponsors', 'Finance\SponsorController', ['except' => ['create', 'edit']]);
        Route::resource('/fees', 'Finance\FeeController', ['except' => ['create', 'edit']]);

        // PAYMENT
        Route::get('/payments/summary/chapter', 'Finance\PaymentController@summaryChapter');
        Route::resource('/payments', 'Finance\PaymentController');

        // MEMBER
        Route::group(['prefix' => '/members/{member}'], function() {
            // Route::get('/dues', 'Finance\MemberDueController@index');
            Route::get('/payments', 'Finance\MemberPaymentController@index');
        });
    });
});

/*
Route::group(['middleware' => 'api'], function() {
    Route::resource('/mail-codes', 'MailCodeController');
    Route::resource('/certificates', 'CertificateController');
});
*/
