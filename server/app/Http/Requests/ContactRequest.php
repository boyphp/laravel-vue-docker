<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|max:255',
            'office_no' => 'max:64',
            'home_no' => 'max:64',
            'mobile_no' => 'max:64',
            'fax_no' => 'max:64',
            'secondary_email' => 'max:255',
            'type' => 'max:32',
            'remarks' => 'max:255',
        ];
    }
}
