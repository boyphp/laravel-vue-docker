<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'last_name' => 'required|max:128',
            'first_name' => 'required|max:128',
            'chapter_id' => 'required|integer',
            'category_id' => 'required|integer',

            'address.name' => 'sometimes|required',
            'contact.email' => 'sometimes|required',
        ];
    }
}
