<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ActivityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:128',
            'code' => 'nullable|max:16',
            'date_from' => 'required|date',
            'date_to' => 'required|date',
            'venue' => 'nullable|max:128',
            'units' => 'required|integer',
            'type_id' => 'required|integer',
            'fiscal_id' => 'nullable|integer',
            'organizer' => 'nullable|max:128',
            'remarks' => 'nullable|max:255',
        ];
    }
}
