<?php

namespace App\Http\Requests\Finance;

use App\Models\Finance\PayerType;
use App\Models\Finance\PaymentMode;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'transaction_no' => 'required',
            'or_no' => 'required',
            'payer_type_id' => 'required|integer',
            'payer_name' => 'required|max:128',
            'mode_id' => 'required|integer',
            'amount' => 'required|integer',
            'total_amount' => 'required|integer',

            'particulars' => 'sometimes|array',
            'particulars.*.particular_id' => 'required|integer',
            'particulars.*.amount_paid' => 'required',
        ];
    }

    public function withValidator($validator)
    {
        // todo: use Rule
        $validator->sometimes(['member_id'], 'required', function($input) {
            return PayerType::where(['id' => $input->payer_type_id, 'name' => 'MEMBER'])->count();
        });

        $validator->sometimes(['check_no', 'check_date', 'check_amount'], 'required', function($input) {
            return PaymentMode::where(['id' => $input->mode_id, 'name' => 'CHECK'])->count();
        });
    }
}
