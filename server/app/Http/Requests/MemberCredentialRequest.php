<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MemberCredentialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'prc_no' => 'required|max:64',
            'prc_date_issued' => 'required|max:64',
            'pma_no' => 'nullable|max:64',
            'pma_date_issued' => 'required_with:pma_no|max:64',
            'pma_expiry_date' => 'required_with:pma_no|max:64',
            'phealth_no' => 'nullable|max:64',
            'phealth_expiry_date' => 'required_with:phealth_no|max:64',
            'provider_no' => 'nullable|max:64',
            'provider_expiry_date' => 'required_with:provider_no|max:64',
        ];
    }
}
