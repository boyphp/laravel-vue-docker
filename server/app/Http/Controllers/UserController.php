<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use App\Auth\Register;

class UserController extends Controller
{

	public function __construct()
	{

	}

	public function index()
	{
		  $users = User::all();
          return view('admin.user.index',compact('users'));
	}

    public function profile(Request $request)
    {
    	return $request->user();
    }

    public function create()
    {
    	 $roles = Role::all();
        return view('admin.user.create',compact('roles'));
    }

    public function store(Request $request)
    {
        $user = User::create($request->except(['permission','_token']));
        $user->roles()->sync($request->roles);
        return redirect()->route('user.index')->withMessage('User successfuly added');
    }

    public function edit($id)
    {
    	$user = User::find($id);
        $roles = Role::all();
        $role_user = $user->roles()->pluck('id','id')->toArray();
        return view('admin.user.edit',compact('user','roles','role_user'));
    }

    public function update(Request $request, $id)
    {
        $user=User::find($id);
        $user->roles()->sync($request->roles);
        return redirect()->route('user.index')->withMessage('User successfully updated');
    }
}
