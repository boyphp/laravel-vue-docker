<?php

namespace App\Http\Controllers\Finance;

use App\Http\Requests\CatalogRequest;
use App\Models\Finance\PayerType;
use Illuminate\Http\Request;

class PayerTypeController extends Controller
{
    public function index()
    {
        return PayerType::all();
    }

	public function store(CatalogRequest $request)
	{
		return PayerType::create($request->all());
	}

    public function update(CatalogRequest $request, PayerType $type)
    {
        $type->update($request->all());

        return $type;
    }

    public function destroy(PayerType $type)
    {
        return ['deleted' => $type->delete()];
    }
}
