<?php

namespace App\Http\Controllers\Finance;

use App\Http\Requests\Finance\PaymentRequest;
use App\Models\Finance\Payment;
use App\Repositories\Finance\PaymentRepository;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    protected $repository;

    public function __construct(PaymentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        return $this->repository->search($request);
    }

    public function summaryChapter()
    {
        return $this->repository->aggregateMonthlyAmount();
    }

	public function store(PaymentRequest $request)
	{
        $payment = Payment::create($request->all());

        if ($particulars = $request->input('particulars')) {
            //
            $payment->particulars()->createMany($particulars);
        }

        return $payment;
	}
}
