<?php

namespace App\Http\Controllers\Finance;

use App\Http\Requests\Finance\SponsorRequest;
use App\Models\Finance\Sponsor;
use Illuminate\Http\Request;

class SponsorController extends Controller
{
    public function index()
    {
        return Sponsor::all();
    }

    public function store(SponsorRequest $request)
    {
        $sponsor = Sponsor::create($request->all());

        $sponsor->address()->create($request->get('address'));

        $sponsor->contact()->create($request->get('contact'));

        return $sponsor->load(['address', 'contact']);
    }

    public function update(SponsorRequest $request, Sponsor $sponsor)
    {
        $sponsor->update($request->all());

        $sponsor->address()->updateOrCreate([], $request->get('address'));

        $sponsor->contact()->updateOrCreate([], $request->get('contact'));

        return $sponsor->load(['address', 'contact']);
    }

    public function destroy(Sponsor $sponsor)
    {
        return ['deleted' => $sponsor->delete()];
    }
}
