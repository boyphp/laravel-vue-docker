<?php

namespace App\Http\Controllers\Finance;

use App\Http\Requests\CatalogRequest;
use App\Models\Finance\PaymentMode;
use Illuminate\Http\Request;

class PaymentModeController extends Controller
{
    public function index()
    {
        return PaymentMode::all();
    }

    public function store(CatalogRequest $request)
    {
        return PaymentMode::create($request->all());
    }

    public function update(CatalogRequest $request, PaymentMode $mode)
    {
        $mode->update($request->all());

        return $mode;
    }

    public function destroy(PaymentMode $mode)
    {
        return ['deleted' => $mode->delete()];
    }
}
