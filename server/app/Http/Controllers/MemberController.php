<?php

namespace App\Http\Controllers;

use App\Models\Member;
use App\Http\Requests\MemberRequest;
use App\Repositories\MemberRepository;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    public function __construct(MemberRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        return $this->repository->search($request);
    }

    public function recent()
    {
        return $this->repository->recentlyAdded();
    }

    public function summaryCategory()
    {
        return $this->repository->aggregateTotal();
    }

    public function show(Member $member)
    {
        return $member->load([
            'credential',
            'address',
            'contact',
            'education',
            'hospitals',
            'internships',
            'positions',
            'inductions',
            'societies',
            'exams',
        ]);
    }

    public function store(MemberRequest $request)
    {
        $member = Member::create($request->all());

        return $member->load(['address', 'contact']);
    }

    public function update(MemberRequest $request, Member $member)
    {
        $member->update($request->all());

        if ($address = $request->input('address')) {
            $member->address()->updateOrCreate([], $address);
        }

        if ($contact = $request->input('contact')) {
            $member->contact()->updateOrCreate([], $contact);
        }

        return $member->load(['address', 'contact']);
    }

    public function destroy(Member $member)
    {
        return ['deleted' => $member->delete()];
    }
}
