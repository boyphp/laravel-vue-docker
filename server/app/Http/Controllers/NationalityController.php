<?php

namespace App\Http\Controllers;

use App\Http\Requests\NationalityRequest;
use App\Models\Nationality;
use Illuminate\Http\Request;

class NationalityController extends Controller
{
    public function index()
    {
       return Nationality::all();
    }

    public function store(NationalityRequest $request)
    {
        return Nationality::create($request->all());
    }

    public function update(NationalityRequest $request, Nationality $nationality)
    {
        $nationality->update($request->all());

        return $nationality;
    }

    public function destroy(Nationality $nationality)
    {
        return ['deleted' => $nationality->delete()];
    }
}
