<?php

namespace App\Http\Controllers;

use App\Http\Requests\MemberCredentialRequest;
use App\Models\Member;
use App\Models\MemberCredential;
use Illuminate\Http\Request;

class MemberCredentialController extends Controller
{
    public function index(Member $member)
    {
        return $member->credential;
    }

    public function store(MemberCredentialRequest $request, Member $member)
    {
        return $member->credential()->updateOrCreate([], $request->all());
    }
}
