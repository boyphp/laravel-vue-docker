<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class CategoryScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->where('category', $model->category);
        if ($model->subcategory) {
            $builder->where('subcategory', $model->subcategory);
        }
    }
}