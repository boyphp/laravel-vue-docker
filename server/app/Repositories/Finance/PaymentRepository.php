<?php

namespace App\Repositories\Finance;

use App\Models\Finance\Payment;
use Illuminate\Support\Facades\DB;

class PaymentRepository
{
    public function search($request)
    {
        return Payment::orderBy('amount', 'DESC')->get();
    }

    public function aggregateMonthlyAmount()
    {
        $payments = Payment::onlyWith(['chapter'])
            ->select('chapter_id', DB::raw('MONTH(`payment_date`) AS month'), DB::raw('SUM(`total_amount`) AS total'))
            ->whereRaw("YEAR(`payment_date`) = YEAR(NOW())")
            ->groupBy(['chapter_id', DB::raw('MONTH(`payment_date`)')])->get();

        return $payments->groupBy('chapter_id')->map(function($item, $key) {
            return [
                'chapter' => $item->first()->chapter,
                'months' => $item->pluck('total', 'month'),
            ];
        });
    }
}
