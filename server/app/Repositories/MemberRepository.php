<?php

namespace App\Repositories;

use App\Models\Member;
use Illuminate\Support\Facades\DB;

class MemberRepository
{
    public function search($request, $limit = 40)
    {
        return Member::with(['address', 'contact'])->where(function($builder) use ($request) {
            $query = $request->get('query');
            $builder->whereRaw("CONCAT(`last_name`, `first_name`) LIKE '%{$query}%'");
        })/*->orderBy(['last_name', 'first_name'])*/->take($limit)->get();
    }

    public function recentlyAdded($limit = 10)
    {
        return Member::orderBy('id', 'DESC')->take($limit)->get();
    }

    public function aggregateTotal()
    {
        return Member::onlyWith('category')
            ->select('category_id', DB::raw('COUNT(*) AS total'))
            ->groupBy('category_id')->get();
    }
}
