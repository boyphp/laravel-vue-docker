<?php

namespace App\Models;

use App\Observers\ModelObserver;
use Illuminate\Database\Eloquent\Model as BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Model extends BaseModel
{
    use SoftDeletes;

    protected static function boot()
    {
        parent::boot();

        static::observe(ModelObserver::class);
    }

    public function scopeOnlyWith($query, $with = [])
    {
        return $query->setEagerLoads([])->with($with);
    }
}
