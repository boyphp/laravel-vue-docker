<?php

namespace App\Models;

use App\Models\Catalog as Model;

class MemberCategory extends Model
{
    protected $attributes = ['category' => 'member', 'subcategory' => 'category'];
}
