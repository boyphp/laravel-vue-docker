<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MemberActivity extends Model
{
    use SoftDeletes;

    protected $table = 'MemberActivity';

    protected $fillable = [
    	'activity_id',
    	'date_registered',
    	'remarks',
    ];
}
