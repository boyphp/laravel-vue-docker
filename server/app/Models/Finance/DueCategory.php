<?php

namespace App\Models\Finance;

use App\Models\Finance\Catalog as Model;

class DueCategory extends Model
{
    protected $attributes = ['category' => 'due', 'subcategory' => 'category'];
}
