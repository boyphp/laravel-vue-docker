<?php

namespace App\Models\Finance;

use App\Models\Model;
use App\Models\Activity;

class ActivityFee extends Model
{
    protected $table = 'FinanceActivityFee';

    protected $fillable = ['discount'];

    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }

    public function fee()
    {
        return $this->belongsTo(Fee::class);
    }
}
