<?php

namespace App\Models\Finance;

use App\Models\Finance\Catalog as Model;

class DueStatus extends Model
{
    protected $attributes = ['category' => 'due', 'subcategory' => 'status'];
}
