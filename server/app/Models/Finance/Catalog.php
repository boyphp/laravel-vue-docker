<?php

namespace App\Models\Finance;

use App\Scopes\CategoryScope;
use App\Models\Model;

class Catalog extends Model
{
    protected $table = 'FinanceCatalog';

    protected $fillable = ['code', 'name', 'description'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CategoryScope);
    }
}
