<?php

namespace App\Models;

class Member extends Model
{
    protected $table = 'Member';

    protected $fillable = [
        'title',
        'first_name',
        'last_name',
        'middle_name',
        'extension_name',
        'gender',
        'birthday',
        'nationality_id',
        'marital_status_id',
        'spouse_name',
        'membership_status_id',
        'chapter_id',
        'category_id',
        'year_inducted',
        'subspecialty_id',
        'remarks',
    ];

    protected $with = [
        'nationality',
        'membershipStatus',
        'maritalStatus',
        'category',
        'chapter',
        'subspecialty',
        'credential',
    ];

    public function nationality()
    {
        return $this->belongsTo(Nationality::class);
    }

    public function maritalStatus()
    {
        return $this->belongsTo(MaritalStatus::class);
    }

    public function membershipStatus()
    {
        return $this->belongsTo(MembershipStatus::class);
    }

    public function category()
    {
        return $this->belongsTo(MemberCategory::class);
    }

    public function chapter()
    {
        return $this->belongsTo(MemberChapter::class);
    }

    public function subspecialty()
    {
        return $this->belongsTo(Subspecialty::class);
    }

    public function credential()
    {
        return $this->hasOne(MemberCredential::class);
    }

    public function address()
    {
        return $this->morphOne(Address::class, 'related');
    }

    public function contact()
    {
        return $this->morphOne(Contact::class, 'related');
    }


    public function education()
    {
        return $this->hasMany(MemberEducation::class);
    }

    public function hospitals()
    {
        return $this->hasMany(MemberHospital::class);

    }

    public function internships()
    {
        return $this->hasMany(MemberInternship::class);
    }

    public function positions()
    {
        return $this->hasMany(MemberPosition::class);
    }

    public function inductions()
    {
        return $this->hasMany(MemberInduction::class);
    }

    public function societies()
    {
        return $this->hasMany(MemberSociety::class);
    }

    public function exams()
    {
        return $this->hasMany(MemberExam::class);
    }

    /*public function activities()
    {
        return $this->hasMany(MemberActivity::class);
    }*/
}
