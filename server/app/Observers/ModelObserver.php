<?php

namespace App\Observers;

use Auth;
use App\Models\Model;

class ModelObserver
{
    public function creating(Model $model)
    {
        $model->created_by = Auth::id() ?: 1;
    }

    public function updating(Model $model)
    {
        $model->updated_by = Auth::id();
    }
}
